var currentURL = window.location.href;

var targetURL = "../"; 

var xhr = new XMLHttpRequest();
xhr.open("GET", targetURL, true);
xhr.onreadystatechange = function() {
  if (xhr.readyState === 4 && xhr.status === 200) {
    var responseHTML = xhr.responseText;
    var parser = new DOMParser();
    var responseDoc = parser.parseFromString(responseHTML, "text/html");
    var clonedElement = responseDoc.getElementById("embed").cloneNode(true);

    var imgPorts = clonedElement.querySelectorAll(".img-port.video-container");
    for (var i = imgPorts.length - 1; i >= 0; i--) {
      var link = imgPorts[i].querySelector("a");
      if (link && link.href === currentURL) {
        imgPorts[i].parentNode.removeChild(imgPorts[i]);
      }
    }

    var galleryPort = clonedElement.querySelector(".gallery-port");
    var imgPortsArray = Array.from(galleryPort.querySelectorAll(".img-port.video-container"));
    imgPortsArray.sort(function() {
      return 0.5 - Math.random();
    });

    var numElementsToShow = 4; 

    galleryPort.innerHTML = '';

    for (var i = 0; i < numElementsToShow && i < imgPortsArray.length; i++) {
      galleryPort.appendChild(imgPortsArray[i]);
    }

    var importedContainer = document.getElementById("imported");
    importedContainer.innerHTML = clonedElement.innerHTML;
  }
};
xhr.send();
