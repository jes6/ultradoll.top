window.addEventListener('DOMContentLoaded', (event) => {
  const currentUrl = window.location.href;

  const xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      const parser = new DOMParser();
      const doc = parser.parseFromString(this.responseText, 'text/html');
      const menuContent = doc.querySelector('nav').innerHTML;

      document.querySelector('nav').innerHTML = menuContent;

      const menuItems = document.querySelectorAll('nav a');
      for (let i = 0; i < menuItems.length; i++) {
        const menuItem = menuItems[i];
        menuItem.classList.remove('active');
      }

      for (let i = 0; i < menuItems.length; i++) {
        const menuItem = menuItems[i];

        const menuUrl = menuItem.href;

        if (currentUrl === menuUrl) {
          menuItem.classList.add('active');

          const parentLi = menuItem.parentElement;
          const parentUl = parentLi.parentElement;

          const siblingLis = parentUl.querySelectorAll('li');
          siblingLis.forEach((siblingLi) => {
            if (siblingLi !== parentLi) {
              siblingLi.classList.remove('active');
            }
          });

          const siblingUls = parentUl.parentElement.querySelectorAll('ul');
          siblingUls.forEach((siblingUl) => {
            if (siblingUl !== parentUl) {
              siblingUl.classList.remove('active');
            }
          });
        }
      }

      const icon = document.getElementById('icon');
      const menu = document.querySelector('nav ul');
      icon.addEventListener('click', () => {
        menu.classList.toggle('show');
      });
    }
  };
  xhttp.open('GET', '/index.html', true); 
  xhttp.send();
});
