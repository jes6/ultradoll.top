    const imagesPerPage = 10; 
    let currentPage = 1; 

    function showImages(page) {
      const galleryItems = document.querySelectorAll('.img-port');
      galleryItems.forEach((item, index) => {
        if (index >= (page - 1) * imagesPerPage && index < page * imagesPerPage) {
          item.style.display = 'block'; 
        } else {
          item.style.display = 'none'; 
        }
      });
    }

    function createPaginationNumbers() {
      const pagination = document.querySelector('.pagination');
      pagination.innerHTML = '';

      const galleryItems = document.querySelectorAll('.img-port');
      const totalImages = galleryItems.length;

      const totalPages = Math.ceil(totalImages / imagesPerPage);

      for (let i = 1; i <= totalPages; i++) {
        const pageNumber = document.createElement('span');
        pageNumber.textContent = i;
        pageNumber.classList.add('pagination-item');
        if (i === currentPage) {
          pageNumber.classList.add('active'); 
        }
        pageNumber.addEventListener('click', function () {
          currentPage = i; 
          showImages(currentPage); 
          updatePagination(); 
        });
        pagination.appendChild(pageNumber);
      }
    }

    function updatePagination() {
      const paginationItems = document.querySelectorAll('.pagination-item');
      paginationItems.forEach((item) => {
        const pageNumber = parseInt(item.textContent);
        if (pageNumber === currentPage) {
          item.classList.add('active'); 
        } else {
          item.classList.remove('active'); 
        }
      });
    }

    showImages(currentPage);

    createPaginationNumbers();
