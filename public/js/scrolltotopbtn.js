function scrollToTop() {
  window.scrollTo({ top: 0, behavior: 'smooth' });
}

document.querySelector('.scrollToTopBtn').addEventListener('click', scrollToTop);
