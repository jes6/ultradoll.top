function setCookie(name, value, days) {
  const expires = new Date();
  expires.setTime(expires.getTime() + days * 24 * 60 * 60 * 1000);
  document.cookie = `${name}=${value}; expires=${expires.toUTCString()}; path=/; SameSite=None; Secure`;
}

function getCookie(name) {
  const cookieName = `${name}=`;
  const cookieArray = document.cookie.split(';');
  for (let i = 0; i < cookieArray.length; i++) {
    let cookie = cookieArray[i];
    while (cookie.charAt(0) === ' ') {
      cookie = cookie.substring(1);
    }
    if (cookie.indexOf(cookieName) === 0) {
      return cookie.substring(cookieName.length, cookie.length);
    }
  }
  return null;
}

function addCookieElement() {
  const cookieBox = document.createElement("div");
  cookieBox.className = "galleta";
  cookieBox.innerHTML = `
    <img src="https://ultradoll.top/img/galletas-o-cookies.jpg" alt="Animate cookie image." loading="lazy" width="150" height="150"/>
    <div class="contenedor-galleta">
      <p class="second">Cookies Consent</p>
      <p>This website uses cookies to ensure you the best experience. Thank you!</p>
      <div class="buttons">
        <button class="item" id="acceptCookies">Understood!</button>
        <a class="item" href="https://ultradoll.top/cookies-policy/" rel="follow" target="_blank">Want to learn more?</a>
      </div>
    </div>
  `;

  document.body.appendChild(cookieBox);

  const acceptBtn = cookieBox.querySelector("#acceptCookies");
  acceptBtn.onclick = () => {
    setCookie("cookieConsent", "accepted", 365);
    cookieBox.classList.add("hide");
  };
}

const checkCookie = getCookie("cookieConsent");

if (!checkCookie) {
  addCookieElement();
}
